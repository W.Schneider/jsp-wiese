package de.wsc.cli;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CliArgsTest {

    @Test
    void getAndRemove() {
        List<String> args = Stream.of("-a", "albert", "-be", "berta", "birgit", "--pass", "h-").collect(Collectors.toList());
        List<String> expected = List.of("-be", "berta", "birgit", "--pass", "h-");
        assertEquals("albert", CliArgs.getAndRemove("-a", args));
        assertEquals(expected, args);

        assertNull(CliArgs.getAndRemove("-a", Stream.of("-a").collect(Collectors.toList())));
        assertNull(CliArgs.getAndRemove("-a", Stream.of("a").collect(Collectors.toList())));
        assertNull(CliArgs.getAndRemove("-a", new ArrayList<>()));
        assertNull(CliArgs.getAndRemove("-a", null));

        assertNull(CliArgs.getAndRemove("-a", Stream.of("-a", "-b").collect(Collectors.toList())));
        assertNull(CliArgs.getAndRemove("-a", Stream.of("-a", "--be").collect(Collectors.toList())));
    }

    @Test
    void hasAndRemove() {
        assertTrue(CliArgs.hasAndRemove("-a", Stream.of("-a", "albert", "-be", "berta", "birgit", "--pass", "h-").collect(Collectors.toList())));
        assertFalse(CliArgs.hasAndRemove("-aa", Stream.of("-a", "albert", "-be", "berta", "birgit", "--pass", "h-").collect(Collectors.toList())));
        assertFalse(CliArgs.hasAndRemove("-a", null));
        assertFalse(CliArgs.hasAndRemove("-a", Collections.emptyList()));
    }

    @Test
    void hasOptions() {
        assertTrue(CliArgs.hasOptions(Arrays.asList("-a", "albert", "-be", "berta", "birgit", "--pass", "h-")));
        assertFalse(CliArgs.hasOptions(Arrays.asList("a", "albert", "be", "berta", "birgit", "--pass", "h-")));
        assertFalse(CliArgs.hasOptions(null));
        assertFalse(CliArgs.hasOptions(Collections.emptyList()));
    }

    @Test
    void getOptions() {
        List<String> args = Arrays.asList("-a", "albert", "-be", "berta", "birgit", "--pass", "h-");
        List<String> expected = Arrays.asList("-a", "-be");
        assertEquals(expected, CliArgs.getOptions(args));

        assertTrue(CliArgs.getOptions(null).isEmpty());
        assertTrue(CliArgs.getOptions(Collections.emptyList()).isEmpty());
        assertTrue(CliArgs.getOptions(Arrays.asList("albert", "berta")).isEmpty());
    }

    @Test
    void getAndRemoveConfigsTest() {
        List<String> args = Stream.of("--d==", "-a", "albert", "--skip", "--user=john", "--pass=", "--header=auth=basic").collect(Collectors.toList());
        List<String> expected = Stream.of("-a", "albert").collect(Collectors.toList());
        Map<String, String> configs = CliArgs.getAndRemoveConfigs(args);

        assertEquals(expected, args);

        assertEquals("true", configs.get("skip"));
        assertEquals("john", configs.get("user"));
        assertEquals("auth=basic", configs.get("header"));
        assertEquals("=", configs.get("d"));
        assertEquals("", configs.get("pass"));
    }
}