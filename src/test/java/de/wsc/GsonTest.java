package de.wsc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.wsc.gson.LocalDateTimeAdapter;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GsonTest {

    public static class DateTime {
        private final LocalDateTime date;

        public DateTime(LocalDateTime date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "DateTime{" +
                    "date=" + date +
                    '}';
        }
    }

    @Test
    public void toLocalDateTimeTest() {
        DateTime dateTime = new DateTime(LocalDateTime.of(2025, 1, 25, 9, 36, 23));
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        String json = gson.toJson(dateTime);
        System.out.printf(json);
    }

    @Test
    public void fromLocalDateTimeTest() {
        String json = "{\"date\":\"2025-01-25T09:24:01Z\"}";
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();
        DateTime jsonElement = gson.fromJson(json, DateTime.class);
        System.out.println(jsonElement);
    }

    @Test
    public void ownLocalDateTimeTest() {
        DateTime dateTime = new DateTime(LocalDateTime.of(2025, 1, 25, 9, 36, 23));
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter(DateTimeFormatter.ISO_DATE_TIME))
                .create();

        String json = gson.toJson(dateTime);
        System.out.printf(json);
    }
}
