<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UERZi start page with grid layout and HTMX</title>
    <script src="https://unpkg.com/htmx.org@2.0.1" integrity="sha384-QWGpdj554B4ETpJJC9z+ZHJcA/i59TyjxEPXiiUgN2WmTyV5OEZWCD6gQhgkdpB/" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body >
<div class="grid grid-cols-[10%_auto]">
    <jsp:include page="jsp/home.jsp"/>
    <jsp:include page="jsp/nav1.jsp"/>
    <jsp:include page="jsp/nav2.jsp"/>
    <jsp:include page="jsp/content.jsp"/>
</div>

</body>
</html>
