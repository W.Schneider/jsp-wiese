<%@ page contentType="text/html;charset=UTF-8" %>

<script>
    function select(element, index) {
        console.log(index);
        console.log(element);
        document.querySelectorAll("#nav1 .bg-red-200").forEach(e => e.classList.remove("bg-red-200"));
        element.classList.add("bg-red-200");
    }
</script>

<nav id="nav1" class="bg-slate-200 flex gap-4 items-end pl-2">
    <button class="bg-slate-300 focus:border-solid" data-hx-get="jsp/content.jsp?from=nav1-eins" data-hx-target="#content" data-hx-swap="outerHTML"
            data-hx-on-click="select(this, 1);"
    >eins</button>
    <button class="bg-slate-300 focus:border-solid" data-hx-get="jsp/content.jsp?from=nav1-zwei" data-hx-target="#content" data-hx-swap="outerHTML"
            data-hx-on-click="select(this, 2);"
    >zwei</button>
</nav>
