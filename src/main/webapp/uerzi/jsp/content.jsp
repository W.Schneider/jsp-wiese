<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="mySpecial" class="de.wsc.MySpecialActivity"/>

<style>
    /* DivTable.com */
    .divTable {
        display: table;
        width: 100%;
    }

    .divTableRow {
        display: table-row;
    }

    .divTableHeading {
        background-color: #EEE;
        display: table-header-group;
    }

    .divTableCell, .divTableHead {
        border: 1px solid #999999;
        display: table-cell;
        padding: 3px 10px;
    }

    .divTableHeading {
        background-color: #EEE;
        display: table-header-group;
        font-weight: bold;
    }

    .divTableFoot {
        background-color: #EEE;
        display: table-footer-group;
        font-weight: bold;
    }

    .divTableBody {
        display: table-row-group;
    }
</style>

<main id="content" class="bg-slate-50">
    <h1>content</h1>
    <c:out value="${param.from}"/>

    <div class="table w-full">
        <div class="md:table-header-group hidden">
            <div class="table-row">
                <div class="table-cell">Objektname</div>
                <div class="table-cell">Objektart</div>
                <div class="table-cell"></div>
                <div class="table-cell">Bezeichnung</div>
                <div class="table-cell">Beschreibung</div>
                <div class="table-cell">Datum</div>
                <div class="table-cell">Laststep</div>
                <div class="table-cell">Lastfunc</div>
                <div class="table-cell">Übergabegrund</div>
                <div class="table-cell md:max-xl:hidden">Gruppierungs&shy;kennzeichen</div>
            </div>
        </div>
        <div class="table-row-group odd:bg-red-300">
            <div class="md:table-row flex flex-col odd:bg-red-300">
                <div class="table-cell"><div class="md:hidden">Objektname: </div>UEJUN1</div>
                <div class="table-cell"><div class="md:hidden">Objektart: </div>PGM</div>
                <div class="table-cell"><div class="md:hidden">S-Version: </div>T</div>
                <div class="table-cell"><div class="md:hidden">Bezeichnung: </div>Testprogramm</div>
                <div class="table-cell"><div class="md:hidden">Beschreibung: </div>Beschreibung kann etwas länger sein</div>
                <div class="table-cell"><div class="md:hidden">Datum: </div>07.09.2024</div>
                <div class="table-cell"><div class="md:hidden">Laststep: </div>Bereitstellung</div>
                <div class="table-cell"><div class="md:hidden">Lastfunc: </div>Erfasst</div>
                <div class="xl:table-cell"><div class="md:hidden">Übergabegrund: </div>Dies ist der Übergabegrund</div>
                <div class="table-cell md:max-xl:hidden"><div class="md:hidden">Gruppierungs&shy;kennzeichen: </div>Dies ist das Gruppierungskennzeichen</div>
            </div>
            <div class="md:table-row flex flex-col odd:bg-red-300">
                <div class="table-cell"><div class="md:hidden">Objektname: </div>UEJ9I</div>
                <div class="table-cell"><div class="md:hidden">Objektart: </div>EAR</div>
                <div class="table-cell"><div class="md:hidden">S-Version: </div></div>
                <div class="table-cell"><div class="md:hidden">Bezeichnung: </div>Testprogramm</div>
                <div class="table-cell"><div class="md:hidden">Beschreibung: </div>Beschreibung kann etwas länger sein</div>
                <div class="table-cell"><div class="md:hidden">Datum: </div>07.09.2024</div>
                <div class="table-cell"><div class="md:hidden">Laststep: </div>Bereitstellung</div>
                <div class="table-cell"><div class="md:hidden">Lastfunc: </div>Erfasst</div>
                <div class="xl:table-cell"><div class="md:hidden">Übergabegrund: </div>Dies ist der Übergabegrund</div>
                <div class="table-cell md:max-xl:hidden"><div class="md:hidden">Gruppierungs&shy;kennzeichen: </div>Dies ist das Gruppierungskennzeichen</div>
            </div>
            <div class="md:table-row flex flex-col odd:bg-red-300">
                <div class="table-cell"><div class="md:hidden">Objektname: </div>UEJUN1</div>
                <div class="table-cell"><div class="md:hidden">Objektart: </div>PGM</div>
                <div class="table-cell"><div class="md:hidden">S-Version: </div>T</div>
                <div class="table-cell"><div class="md:hidden">Bezeichnung: </div>Testprogramm</div>
                <div class="table-cell"><div class="md:hidden">Beschreibung: </div>Beschreibung kann etwas länger sein</div>
                <div class="table-cell"><div class="md:hidden">Datum: </div>07.09.2024</div>
                <div class="table-cell"><div class="md:hidden">Laststep: </div>Bereitstellung</div>
                <div class="table-cell"><div class="md:hidden">Lastfunc: </div>Erfasst</div>
                <div class="xl:table-cell"><div class="md:hidden">Übergabegrund: </div>Dies ist der Übergabegrund</div>
                <div class="table-cell md:max-xl:hidden"><div class="md:hidden">Gruppierungskennzeichen: </div>Dies ist das Gruppierungskennzeichen</div>
            </div>

            <div class="md:table-row flex flex-col odd:bg-red-300" data-hx-get="jsp/content.jsp?from=history" data-hx-target="#content" data-hx-swap="outerHTML"
                 data-hx-push-url="true">
                <div class="table-cell"><div class="md:hidden">Objektname: </div>UEJ9I</div>
                <div class="table-cell"><div class="md:hidden">Objektart: </div>EAR</div>
                <div class="table-cell"><div class="md:hidden">S-Version: </div></div>
                <div class="table-cell"><div class="md:hidden">Bezeichnung: </div>Testprogramm</div>
                <div class="table-cell"><div class="md:hidden">Beschreibung: </div>Beschreibung kann etwas länger sein</div>
                <div class="table-cell"><div class="md:hidden">Datum: </div>07.09.2024</div>
                <div class="table-cell"><div class="md:hidden">Laststep: </div>Bereitstellung</div>
                <div class="table-cell"><div class="md:hidden">Lastfunc: </div>Erfasst</div>
                <div class="xl:table-cell"><div class="md:hidden">Übergabegrund: </div>Dies ist der Übergabegrund</div>
                <div class="table-cell md:max-xl:hidden"><div class="md:hidden">Gruppierungskennzeichen: </div>Dies ist das Gruppierungskennzeichen</div>
            </div>
        </div>
    </div>
</main>