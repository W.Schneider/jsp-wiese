<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav id="nav2" class="flex flex-col bg-slate-200 items-center">
    <button class="bg-slate-300 focus:border-solid" data-hx-get="jsp/content.jsp?from=nav2-eins" data-hx-target="#content" data-hx-swap="outerHTML">eins</button>
    <button class="bg-slate-300 focus:border-solid" data-hx-get="jsp/content.jsp?from=nav2-zwei" data-hx-target="#content" data-hx-swap="outerHTML">zwei</button>
</nav>
<c:out value="${param.from}"/>
