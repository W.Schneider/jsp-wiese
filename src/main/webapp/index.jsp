<!DOCTYPE html/>
<html lang="de">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="hello" class="de.wsc.Hello" scope="request"/>
<jsp:useBean id="properties" class="de.wsc.WieseProperties" scope="application"/>
<jsp:useBean id="wiesenBean" class="de.wsc.WiesenBean" scope="request"/>

<head>
    <title>jsp wiese</title>
</head>

<body>
<h2>Hello World!</h2>
<h3>${hello.hello}</h3>

<br/>
Hello called ${hello.counter} times;
<br/>
<p>
    Hello! How are you today?
</p>
<p>
    WiesenBean: ${wiesenBean.hello}
</p>
<p>
    WieseProperties: ${properties.hello}
</p>

<p>
    <%--@elvariable id="propertyDao" type="de.wsc.db.PropertyDao"--%>
    <c:forEach items="${propertyDao.instance.properties}" var="prop">
        PropertyDao: ${prop}<br/>
    </c:forEach>
</p>

<a href="uerz.html">uerz.html</a>
<a href="cols.html">cols.html</a>
</body>
</html>
