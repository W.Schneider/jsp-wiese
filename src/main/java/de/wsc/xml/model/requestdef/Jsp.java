package de.wsc.xml.model.requestdef;

import javax.xml.bind.annotation.XmlAttribute;

public class Jsp {
    @XmlAttribute
    private String url;

    public String getUrl() {
        return url;
    }
}
