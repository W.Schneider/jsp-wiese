package de.wsc.xml.model.requestdef;

import javax.xml.bind.annotation.*;
import java.util.List;

public class Request {

    public enum History {no, back, list}

    private String desc;
    private Authorization authorization;
    private Activity activity;
    private List<Validator> validators;
    private Jsp jsp;

    @XmlAttribute
    private String name;
    @XmlAttribute
    private boolean hidden;
    @XmlAttribute
    private History history;
    @XmlAttribute
    private boolean alwaysValidate;


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    public Jsp getJsp() {
        return jsp;
    }

    public void setJsp(Jsp jsp) {
        this.jsp = jsp;
    }

    public List<Validator> getValidators() {
        return validators;
    }

    @XmlElement(name = "validator")
    public void setValidators(List<Validator> validators) {
        this.validators = validators;
    }

    public String getName() {
        return name;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean isHidden() {
        return hidden;
    }

    public History getHistory() {
        return history;
    }

    public boolean isAlwaysValidate() {
        return alwaysValidate;
    }
}


