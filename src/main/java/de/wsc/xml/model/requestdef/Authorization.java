package de.wsc.xml.model.requestdef;

import javax.xml.bind.annotation.XmlAttribute;

public class Authorization {
    @XmlAttribute
    private boolean everybody;
    @XmlAttribute
    private boolean admin;
    @XmlAttribute
    private boolean soko;
    @XmlAttribute
    private boolean config;
    @XmlAttribute
    private boolean dvz;
    @XmlAttribute
    private boolean editor;
    @XmlAttribute
    private boolean subeditor;
    @XmlAttribute
    private boolean parabind;
    @XmlAttribute
    private boolean user;
    @XmlAttribute
    private boolean guest;

    public boolean isEverybody() {
        return everybody;
    }

    public boolean isAdmin() {
        return admin;
    }

    public boolean isSoko() {
        return soko;
    }

    public boolean isConfig() {
        return config;
    }

    public boolean isDvz() {
        return dvz;
    }

    public boolean isEditor() {
        return editor;
    }

    public boolean isSubeditor() {
        return subeditor;
    }

    public boolean isParabind() {
        return parabind;
    }

    public boolean isUser() {
        return user;
    }

    public boolean isGuest() {
        return guest;
    }
}
