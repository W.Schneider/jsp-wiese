package de.wsc.xml.model.requestdef;

import javax.xml.bind.annotation.XmlAttribute;

public class Activity {
    @XmlAttribute(name = "class")
    private String clazz;

    public String getClazz() {
        return clazz;
    }
}
