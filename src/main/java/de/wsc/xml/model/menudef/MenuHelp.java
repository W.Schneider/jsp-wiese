package de.wsc.xml.model.menudef;

import javax.xml.bind.annotation.XmlAttribute;

public class MenuHelp {
    @XmlAttribute
    private String primary;
    @XmlAttribute
    private String secondary;
    @XmlAttribute
    private String displaytext;
    @XmlAttribute
    private String hint;
    @XmlAttribute
    private String target;
    @XmlAttribute
    private boolean launchExtIcon;
    @XmlAttribute
    private boolean proxy;

    public String getPrimary() {
        return primary;
    }

    public String getSecondary() {
        return secondary;
    }

    public String getDisplaytext() {
        return displaytext;
    }

    public String getHint() {
        return hint;
    }

    public String getTarget() {
        return target;
    }

    public boolean isLaunchExtIcon() {
        return launchExtIcon;
    }

    public boolean isProxy() {
        return proxy;
    }
}
