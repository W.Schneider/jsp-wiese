package de.wsc.xml.model.menudef;

import de.wsc.xml.model.requestdef.Authorization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class App {
    @XmlAttribute
    private String name;

    @XmlElement
    private Authorization authorization;

    @XmlElement(name="menuinternal")
    private List<MenuInternal> menuInternal;

    @XmlElement(name="menuexternal")
    private List<MenuExternal> menuExternal;

    @XmlElement(name="menuhelp")
    private List<MenuHelp> menuHelp;

    public String getName() {
        return name;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    public List<MenuInternal> getMenuInternal() {
        return menuInternal;
    }

    public List<MenuExternal> getMenuExternal() {
        return menuExternal;
    }

    public List<MenuHelp> getMenuHelp() {
        return menuHelp;
    }
}
