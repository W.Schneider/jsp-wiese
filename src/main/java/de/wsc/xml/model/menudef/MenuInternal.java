package de.wsc.xml.model.menudef;

import javax.xml.bind.annotation.XmlAttribute;

public class MenuInternal {
    @XmlAttribute
    private String primary;
    @XmlAttribute
    private String secondary;
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String displaytext;
    @XmlAttribute
    private String hint;
    @XmlAttribute
    private boolean launchExtIcon;

    public String getPrimary() {
        return primary;
    }

    public String getSecondary() {
        return secondary;
    }

    public String getName() {
        return name;
    }

    public String getDisplaytext() {
        return displaytext;
    }

    public String getHint() {
        return hint;
    }

    public boolean isLaunchExtIcon() {
        return launchExtIcon;
    }
}
