package de.wsc.xml.model.wizarddef;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class Wizard {
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String flowtype;

    @XmlElementWrapper(name = "values")
    @XmlElement(name = "value")
    private List<Value> values;
    @XmlElement
    private Configurator configurator;
    @XmlElement
    private Steps steps;

    public Wizard() {
    }

    public Steps getSteps() {
        return steps;
    }

    public List<Value> getValues() {
        return values;
    }

    public Configurator getConfigurator() {
        return configurator;
    }

    public String getName() {
        return name;
    }

    public String getFlowtype() {
        return flowtype;
    }
}
