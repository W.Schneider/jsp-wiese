package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Choose {

    @XmlElement(name = "when")
    private List<When> whens;

    @XmlElement
    private Otherwise otherwise;

    public List<When> getWhens() {
        return whens;
    }

    public Otherwise getOtherwise() {
        return otherwise;
    }
}
