package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlAttribute;

public class Step {
    @XmlAttribute
    private int index;
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String title;
    @XmlAttribute
    private String type;
    @XmlAttribute
    private boolean show;

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public boolean isShow() {
        return show;
    }
}
