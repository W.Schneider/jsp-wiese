package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Steps {
    @XmlElement(name = "step")
    private List<Step> steps;

    @XmlElement(name = "choose")
    private List<Choose> chooses;

    public List<Step> getSteps() {
        return steps;
    }

    public List<Choose> getChooses() {
        return chooses;
    }
}
