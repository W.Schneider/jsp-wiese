package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlElement;

public class Otherwise {
    @XmlElement
    private Step step;

    public Step getStep() {
        return step;
    }
}
