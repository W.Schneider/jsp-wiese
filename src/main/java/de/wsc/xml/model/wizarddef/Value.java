package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlAttribute;

public class Value {
    @XmlAttribute
    private String name;

    @XmlAttribute
    private String type;

    @XmlAttribute(name = "default")
    private String defaultValue;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
