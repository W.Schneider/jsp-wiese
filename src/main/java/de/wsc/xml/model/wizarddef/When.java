package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class When {

    @XmlAttribute
    private String test;

    @XmlElement
    private Step step;

    public String getTest() {
        return test;
    }

    public Step getStep() {
        return step;
    }
}
