package de.wsc.xml.model.wizarddef;

import javax.xml.bind.annotation.XmlAttribute;

public class Configurator {
    @XmlAttribute(name = "class")
    private String clazz;

    public String getClazz() {
        return clazz;
    }
}
