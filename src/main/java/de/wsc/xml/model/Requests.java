package de.wsc.xml.model;

import de.wsc.xml.model.menudef.App;
import de.wsc.xml.model.requestdef.Request;
import de.wsc.xml.model.wizarddef.Wizard;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Requests {
    private List<Request> requests;
    private List<Wizard> wizards;

    @XmlElementWrapper(name = "menudef")
    @XmlElement(name = "app")
    private List<App> apps;

    public List<Request> getRequests() {
        return requests;
    }

    @XmlElementWrapper(name = "requestdef")
    @XmlElement(name = "request")
    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    public List<Wizard> getWizards() {
        return wizards;
    }

    @XmlElementWrapper(name = "wizarddef")
    @XmlElement(name = "wizard")
    public void setWizards(List<Wizard> wizards) {
        this.wizards = wizards;
    }

    public List<App> getMenus() {
        return apps;
    }
}
