package de.wsc.xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.wsc.xml.model.Requests;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class JaxbTest {
    public static void main(String[] args) {
        File xmlFile = new File("requests.xml");

        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(Requests.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Requests requests = (Requests) jaxbUnmarshaller.unmarshal(xmlFile);


            System.out.println("requests: " + requests.getRequests().size());
            System.out.println("wizards: " + requests.getWizards().size());
            System.out.println("apps: " + requests.getMenus().size());

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(requests);
            System.out.println(json);
            FileWriter writer = new FileWriter("requests.json");
            writer.write(json);
            writer.close();


        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
