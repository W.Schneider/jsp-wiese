package de.wsc;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class WiesenBean {

    private final JpaAutokostenEntityDao dao;

    public WiesenBean() {
        dao = null;
    }

    @Inject
    public WiesenBean(JpaAutokostenEntityDao dao) {
        this.dao = dao;
    }

    public String getHello() {
        return "Hello Wolfgang! Entities were found: " + dao.getAll().size();
    }

}
