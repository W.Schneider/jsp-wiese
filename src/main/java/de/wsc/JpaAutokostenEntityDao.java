package de.wsc;

import javax.enterprise.context.RequestScoped;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@RequestScoped
public class JpaAutokostenEntityDao implements Dao<AutokostenEntity> {

    //@PersistenceContext(unitName="jpa-unit")
    private final EntityManager entityManager;

    public JpaAutokostenEntityDao() {
        entityManager = EntityManagerUtil.getEntityManager();
    }

    @Override
    public Optional<AutokostenEntity> get(long id) {
        return Optional.ofNullable(entityManager.find(AutokostenEntity.class, id));
    }

    @Override
    public List<AutokostenEntity> getAll() {
        TypedQuery<AutokostenEntity> query = entityManager.createQuery("SELECT e FROM AutokostenEntity e", AutokostenEntity.class);
        return query.getResultList();
    }

    @Override
    public void save(AutokostenEntity user) {
        executeInsideTransaction(manager -> manager.persist(user));
    }

    @Override
    public void update(AutokostenEntity user, String[] params) {
        throw new RuntimeException("not yet implemented");
        //executeInsideTransaction(entityManager -> entityManager.merge(user));
    }

    @Override
    public void delete(AutokostenEntity user) {
        executeInsideTransaction(manager -> manager.remove(user));
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();
            action.accept(entityManager);
            tx.commit();
        }
        catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
    }
}