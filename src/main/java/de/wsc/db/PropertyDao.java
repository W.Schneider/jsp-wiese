package de.wsc.db;

import de.wsc.PropertiesDto;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
@Named(value = "propertyDao")
public class PropertyDao {

    @Resource(lookup = "jdbc/derby")
    private DataSource dataSource;

    public PropertyDao getInstance() {
        return CDI.current().select(PropertyDao.class).get();
    }

    public List<PropertiesDto> getProperties() {
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement =  connection.prepareStatement("select name, wert, datum, id from properties")) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    return getProperties(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private List<PropertiesDto> getProperties(ResultSet resultSet) throws SQLException {
        List<PropertiesDto> propertiesDtos = new ArrayList<>();
        while (resultSet.next()) {
            propertiesDtos.add(map(resultSet));
        }

        return propertiesDtos;
    }

    private PropertiesDto map(ResultSet resultSet) throws SQLException {
        PropertiesDto propertiesDto = new PropertiesDto();
        propertiesDto.setName(resultSet.getString("name"));
        propertiesDto.setWert(resultSet.getString("wert"));
        propertiesDto.setDatum(resultSet.getTimestamp("datum").toLocalDateTime());
        propertiesDto.setId(resultSet.getInt("id"));
        return propertiesDto;
    }
}
