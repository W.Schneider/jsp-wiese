package de.wsc.cli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class CliArgs {
    private CliArgs() {}
    public static String getAndRemove(String option, List<String> args) {
        if (args == null) {
            return null;
        }
        boolean found = false;
        String value = null;
        String foundOption = null;
        for (String arg : args) {
            if (found) {
                value = arg;
                break;
            }
            if (option.equals(arg)) {
                found = true;
                foundOption = option;
            }
        }
        args.remove(foundOption);
        if (isOption(value) || isConfig(value)) {
            return null;
        } else {
            args.remove(value);
            return value;
        }
    }

    public static boolean hasAndRemove(String option, List<String> args) {
        return args != null && args.remove(option);
    }

    public static boolean hasOptions(List<String> args) {
        return args != null && args.stream().anyMatch(CliArgs::isOption);
    }

    public static List<String> getOptions(List<String> args) {
        if (args == null) {
            return new ArrayList<>();
        } else {
            return args.stream().filter(CliArgs::isOption).collect(Collectors.toList());
        }
    }

    public static Map<String, String> getAndRemoveConfigs(List<String> args) {
        Map<String, String> configs = new HashMap<>();
        List<String> configStrings = getAndRemoveConfigStrings(args);
        for (String configString : configStrings) {
            String s = configString.substring(2);
            int posOfEquals = s.indexOf('=');
            if (posOfEquals >= 0) {
                configs.put(s.substring(0, posOfEquals), s.substring(posOfEquals + 1));
            } else {
                configs.put(s, "true");
            }
        }
        return configs;
    }

    private static boolean isOption(String arg) {
        return arg != null && arg.length() >= 2 && arg.charAt(0) == '-' && arg.charAt(1) != '-';
    }

    private static boolean isConfig(String arg) {
        return arg != null && arg.length() >= 3 && arg.startsWith("--");
    }

    private static List<String> getAndRemoveConfigStrings(List<String> args) {
        List<String> configStrings = new ArrayList<>();
        for (String arg : args) {
            if (isConfig(arg)) {
                configStrings.add(arg);
            }
        }
        args.removeAll(configStrings);
        return configStrings;
    }
}
