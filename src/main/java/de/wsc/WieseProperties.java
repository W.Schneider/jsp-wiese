package de.wsc;


import javax.annotation.PostConstruct;
import javax.inject.Singleton;

@Singleton
public class WieseProperties {
    private String hello;

    @PostConstruct
    void init() {
        hello = "init";
    }

    public String getHello() {
        return hello;
    }
}
