package de.wsc;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "properties")
public class PropertiesDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime datum;
    private String name;
    private String wert;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDatum() {
        return datum;
    }

    public void setDatum(LocalDateTime datum) {
        this.datum = datum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWert() {
        return wert;
    }

    public void setWert(String wert) {
        this.wert = wert;
    }

    @Override
    public String toString() {
        return "PropertiesDto{" +
                "id=" + id +
                ", datum=" + datum +
                ", name='" + name + '\'' +
                ", wert='" + wert + '\'' +
                '}';
    }
}
