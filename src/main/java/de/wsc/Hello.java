package de.wsc;


import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

@RequestScoped
public class Hello {

    @Resource(name = "jdbc/h2test")
    private DataSource dataSourceH2;

    private int counter;
    public String getHello() {
        counter++;
        return "hello";
    }

    public int getCounter() {
        return counter;
    }
}
