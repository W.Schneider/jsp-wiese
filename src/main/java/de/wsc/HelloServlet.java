package de.wsc;


import de.wsc.db.PropertyDao;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServlet extends HttpServlet {

    private final PropertyDao propertyDao;

    @Inject
    public HelloServlet(PropertyDao dao) {
        propertyDao = dao;
    }

    /*
    @Resource(lookup = "jdbc/mysqljpadatasource")
    private DataSource dataSource;

    @Resource(name = "jdbc/h2test")
    private DataSource dataSourceH2;
     */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.getOutputStream().print(propertyDao.getProperties().toString());
    }
}
