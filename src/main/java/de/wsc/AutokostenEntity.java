package de.wsc;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Entity
@Table(name = "autokosten")
public class AutokostenEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String beschreibung;

    private BigDecimal betrag;

    private String tags;

    private LocalDate datum;

    private Integer tageskilometer;

    private Integer gesamtkilometer;

    private BigDecimal liter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public BigDecimal getBetrag() {
        return betrag;
    }

    public void setBetrag(BigDecimal betrag) {
        this.betrag = betrag;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    //@DateTimeFormat(pattern = "dd.MM.yyyy")
    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public Integer getTageskilometer() {
        return tageskilometer;
    }

    public void setTageskilometer(Integer tageskilometer) {
        this.tageskilometer = tageskilometer;
    }

    public Integer getGesamtkilometer() {
        return gesamtkilometer;
    }

    public void setGesamtkilometer(Integer gesamtkilometer) {
        this.gesamtkilometer = gesamtkilometer;
    }


    public BigDecimal getLiter() {
        return liter;
    }

    public void setLiter(BigDecimal liter) {
        this.liter = liter;
    }

    @Override
    public String toString() {
        return "AutokostenEntity{" +
                "id=" + id +
                ", beschreibung='" + beschreibung + '\'' +
                ", betrag=" + betrag +
                ", tags='" + tags + '\'' +
                ", datum=" + datum +
                ", tageskilometer=" + tageskilometer +
                ", gesamtkilometer=" + gesamtkilometer +
                ", liter=" + liter +
                '}';
    }

    public BigDecimal getDurchschnitt() {
        if (tageskilometer == null || tageskilometer == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal kilometer = BigDecimal.valueOf(tageskilometer);
        return getLiter().multiply(BigDecimal.valueOf(100)).divide(kilometer, RoundingMode.HALF_DOWN);
    }
}
