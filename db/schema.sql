DROP TABLE IF EXISTS wiese;
-- h2
CREATE TABLE wiese
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    datum       TIMESTAMP     NOT NULL,
    bezeichnung VARCHAR(250)  NOT NULL,
    betrag      DECIMAL(6, 2) NOT NULL,
    kategorie   VARCHAR(250)  NOT NULL
);

-- derby
CREATE TABLE wiese
(
    id          INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (Start with 1, Increment by 1),
    datum       TIMESTAMP     NOT NULL,
    bezeichnung VARCHAR(250)  NOT NULL,
    betrag      DECIMAL(6, 2) NOT NULL,
    kategorie   VARCHAR(250)  NOT NULL
);

CREATE TABLE properties
(
    id    INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (Start with 1, Increment by 1),
    --id    INT AUTO_INCREMENT PRIMARY KEY,
    datum TIMESTAMP    NOT NULL,
    name  VARCHAR(250) NOT NULL,
    wert  VARCHAR(250) NOT NULL
);

insert into properties (datum, name, wert)
values ({ts '2022-03-19 08:00:00'}, 'user', 'wolfgang');

select *
from properties;
